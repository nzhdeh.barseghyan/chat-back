/**
 * Check login credentials for user to do Login in system
 * */
function login(request, reply) {
    console.log(' controllers >> auth >> login', request.body);
    reply.send({message: 'login from auth controller'});
}

/**
 * Create a new user.
 * */
function registerNewUser(request, reply) {
    console.log(' controllers >> auth >> createNewUser', request.body);
    reply.send({message: 'createNewUser from auth controller'});
}


module.exports.login = login;
module.exports.registerNewUser = registerNewUser;