const app = require('./app');
const port = process.env.PORT_ENV | 5000;

app.listen(port, () => {
    console.log('process.env.CURRENT_PATH', process.env.CURRENT_PATH);
    console.log(' Server is running in port ', port);
})