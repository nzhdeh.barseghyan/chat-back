const express = require('express');
const router = express.Router();
const path = require('path');
const authController = require(path.join(process.env.CURRENT_PATH, 'controllers', 'auth.js'));
const userCtrl = require(path.join(process.env.CURRENT_PATH, 'controllers', 'userCtrl.js'));
const common = require(path.join(process.env.CURRENT_PATH, 'commonEvents.js'));
const commonEmitter = common.emitter;


// listen an event
commonEmitter.on('myEmit', () => {
    console.log('Listen an myEmit', commonEmitter.eventNames());
})
/**
 * Login in the system
 * */
router.get('/login', authController.login);
router.get('/register', authController.registerNewUser);
router.get('/verifyEmail/:email', userCtrl.verifyEmail);

module.exports = router;