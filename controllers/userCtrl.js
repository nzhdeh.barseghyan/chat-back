const path = require('path');
const common = require(path.join(process.env.CURRENT_PATH, 'commonEvents.js'));
const commonEmitter = common.emitter;

/**
 *Check if a user exists with the same email address
 * @param {Object} request - The request of the function
 * @param {Object} request.params - The parameters of the function (the data).
 * @param {objectId} request.params.email - The email of the user.
 * @param {Response<any>} reply - The reply function.
 * */
function verifyEmail(request, reply) {
    console.log(' controllers >> userCtrl >> verifyEmail', request.params);
    reply.send({message: 'verifyEmail from userCtrl'});
}

module.exports.verifyEmail = verifyEmail;