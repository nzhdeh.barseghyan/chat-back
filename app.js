const express = require('express');
const app = express();
const path = require('path');
process.env.CURRENT_PATH = __dirname;
const usersRoutes = require(path.join(process.env.CURRENT_PATH, 'routes', 'users.js'));

app.use('/api/users', usersRoutes);

module.exports = app;